var Discord = require('discord.io');
var logger = require('winston');
var mc_proto = require('minecraft-protocol');
var auth = {};
auth.token = process.env.DiscordToken;
var MCServer = {};
MCServer.host = process.env.host;
MCServer.port = process.env.port;
var MCAccount = {};
MCAccount.username = process.env.MCUsername;
MCAccount.password = process.env.MCPassword;

// Configure logger settings
logger.remove(logger.transports.Console);
logger.add(new logger.transports.Console, {
     colorize: true
});
logger.level = 'debug';

// Initialize Discord Bot
var bot = new Discord.Client({
     token: auth.token,
     autorun: true
});

var mc_client = mc_proto.createClient({
     host: MCServer.host,
     port: MCServer.port,
     username: MCAccount.username,
     password: MCAccount.password
});

bot.on('ready', function(evt) {
     logger.info('Connected');
     logger.info('Logged in as: ');
     logger.info(bot.username + ' - (' + bot.id + ')');
     for (var serverID in bot.servers) {
          if (bot.servers.hasOwnProperty(serverID)) {
               var server = bot.servers[serverID];
               logger.info("Joined Server on startup: " + server.name + " (" + serverID + ") in " + server.region);
          }
     }
});
bot.on('message', function(user, userID, channelID, message, evt) {
     //clean message by replacing multiple spaces with a single space and trim the ends
     message = message.trim().replace(/\s\s+/g, ' ');

     if (message.substring(0, 1) == '!') {
          var args = message.substring(1).split(' ');
          var cmd = args[0];
          args = args.splice(1);

          switch (cmd.toLowerCase()) {
               case 'mss':
                    bot.simulateTyping(channelID, function(err, response) {});

                    MCServerPing(MCServer.host, MCServer.port, function(err, data) {
                         if (err) {
                              bot.sendMessage({
                                   to: channelID,
                                   message: "Failed to ping MC Server"
                              });
                              return;
                         }
                         var online = "Total: " + data.players.online + "/" + data.players.max;
                         if (data.players.hasOwnProperty("sample")) {
                              var players = data.players.sample;
                              if (players.length > 1) {
                                   online += "\n";
                              }
                              for (var i = 0; i < players.length; i++) {
                                   online += "\n" + players[i].name;
                              }
                         }

                         bot.sendMessage({
                              to: channelID,
                              message: "",
                              embed: {
                                   //color: "#ffffff",
                                   title: "MC Server Status",
                                   fields: [{
                                             name: "Description",
                                             value: data.description.text
                                        },
                                        {
                                             name: "Currently Online:",
                                             value: online
                                        },
                                        {
                                             name: "MC Version",
                                             value: data.version.name
                                        },
                                        {
                                             name: "Ping",
                                             value: data.ping + "ms"
                                        }
                                   ],
                                   footer: {
                                        text: "© MCServerList"
                                   },
                                   timestamp: new Date(),
                              }
                         });
                    });
                    break;
                    //TODO command to change / add host:port
                    //TODO command to login / re-login to MC server
                    //TODO command to set chat mirror channel. Dont mirror MC chat without channel being set
          }

          return;
     }

     if (channelID === "553357566940545068" && user != bot.username) {
          mc_client.write('chat', {
               message: user + ": " + message
          });

          //FIXME add reation to discord message to indocate message was successfully posted in MC chat
          // bot.addReaction({
          //      channelID: channelID,
          //      messageId: evt.d.id,
          //      reaction: "U+1F44C"
          // }, function(err, res) {
          //      if(err) console.log(err);
          // });
     }
});

bot.on('any', function(evt) {
     switch (evt.t) {
          case 'GUILD_CREATE':
               logger.info("Joined Server: " + evt.d.name + " (" + evt.d.id + ") in " + evt.d.region);
               break;
          case 'GUILD_DELETE':
               logger.info("Left Server: " + evt.d.name + " (" + evt.d.id + ") in " + evt.d.region);
               break;
     }
});

mc_client.on('chat', function(packet) {
     // Listen for chat messages and echo them back.
     var jsonMsg = JSON.parse(packet.message);
     //TODO add messages sent from dynmap chat
     if (jsonMsg.translate == 'chat.type.announcement' || jsonMsg.translate == 'chat.type.text') {
          var username = jsonMsg.with[0].text;
          var msg = jsonMsg.with[1];
          if (username === mc_client.username) return;
          bot.sendMessage({
               to: "553357566940545068",
               message: "",
               embed: {
                    fields: [{
                         name: username,
                         value: msg
                    }]
               }
          })
     }
});

mc_client.on('connect', function() {
     console.log('Successfully connected to ' + MCServer.host + ':' + MCServer.port);
});

//TODO keep attemptin to reconnect if disconected
mc_client.on('disconnect', function(packet) {
     console.log('disconnected: ' + packet.reason);
});
mc_client.on('end', function() {
     console.log('Connection lost');
});
mc_client.on('error', function(err) {
     console.log('Error occured');
     console.log(err);
});

//TODO Move from python to nodejs
function MCServerPing(host, port, cb) {
     const spawn = require("child_process").spawn;
     const pythonProcess = spawn('python3', ["MCServerPing.py", host, port]);
     pythonProcess.stdout.on('data', (data) => {
          data = data.toString().replace(/'/g, "\"");
          data = JSON.parse(data);
          if (!data.hasOwnProperty("version")) {
               cb("Failed to ping MC server", null);
          } else {
               cb(null, data);
          }
     });
}
